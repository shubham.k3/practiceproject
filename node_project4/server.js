const express=require('express');
const app=express();
const bodyParser=require('body-parser');
const path=require('path');
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const join_route=require('./server/routes/join_route');

//cookie parser
var cookieParser = require('cookie-parser')
app.use(cookieParser())

app.set('view engine','pug');
app.use('/',require('./server/routes/routes'));
app.use('/join',join_route);

app.set('views',path.resolve(__dirname,"server/views"));
app.set('images',path.resolve(__dirname,"assets/images"));
app.use(express.static(__dirname+'/assets'));

//const razorpay = require('razorpay');


module.exports = (req,res,next) => {
    const token = req.body.token || req.query.token || req.headers['x-access-token']
    // decode token
    if (token) {
      // verifies secret and checks exp
      jwt.verify(token, config.secret, function(err, decoded) {
          if (err) {
              return res.status(401).json({"error": true, "message": 'Unauthorized access.' });
          }
        req.decoded = decoded;
        next();
      });
    } else {
      // if there is no token
      // return an error
      return res.status(403).send({
          "error": true,
          "message": 'No token provided.'
      });
    }
  }

/*var instance = new razorpay({
  key_id:'YOUR_KEY_ID',
  key_secret:'YOUR_KEY_SECRET'
});*/ 

app.listen(3001,()=>{
    console.log("server running");
});