// Update with your config settings.

module.exports = {

  development: {
    client: 'mysql',
    connection: {
      database: 'node_task1',
      user:     'root',
      password: null,
    },
    pool: {
      min: 2,
      max: 10
    }
  }

};
