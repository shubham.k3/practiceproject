
exports.up = function(knex) {
    return knex.schema
    .createTable('skills', function (table) {
       table.increments('id');
       table.integer('skill_id');
       table.string('skill_name',255).notNullable();
       table.timestamp('created_at', { useTz: true });
       table.engine('InnoDB');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable("developer_skills");
};
exports.config = { transaction: false };