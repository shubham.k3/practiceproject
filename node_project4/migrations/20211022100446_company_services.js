
exports.up = function(knex) {
    return knex.schema
    .createTable('company_services', function (table) {
       table.increments('id');
       table.integer('company_id').notNullable();
       table.integer('service_id').notNullable();
       table.engine('InnoDB');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable("company_services");
};
exports.config = { transaction: false };