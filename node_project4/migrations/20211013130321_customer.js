
exports.up = function(knex) {
    return knex.schema
    .createTable('customer', function (table) {
       table.increments('id');
       table.string('username', 255).notNullable();
       table.string('email', 255).notNullable();
       table.string('password', 255).notNullable();
       table.integer('role').notNullable();
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable("customer");
};
exports.config = { transaction: false };