
exports.up = function(knex) {
    return knex.schema
    .createTable('users', function (table) {
       table.increments('id');
       table.string('first_name',255);
       table.string('last_name',255);
       table.string('user_name',255);
       table.string('phone_no',255).notNullable();
       table.string('email',255);
       table.string('profile_pic',255);
       table.string('city',255);
       table.string('state',255);
       table.integer('status',255).defaultTo(0);
       table.string('password',255).notNullable();
       table.text('token','longtext');
       table.timestamp('created_at').defaultTo(knex.fn.now());
       table.timestamp('updated_at',true);
       table.timestamp('deleted_at',true);
       table.engine('InnoDB');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable("users");
};
exports.config = { transaction: false };