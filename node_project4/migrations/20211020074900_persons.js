
exports.up = function(knex) {
    return knex.schema
    .createTable('persons', function (table) {
       table.increments('id');
       table.string('name', 255).notNullable();
       table.string('email', 255).notNullable();
       table.string('password', 255).notNullable();
       table.integer('gender').notNullable();
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable("persons");
};
exports.config = { transaction: false };