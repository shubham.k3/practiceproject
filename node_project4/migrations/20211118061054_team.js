
exports.up = function(knex) {
    return knex.schema
    .createTable('team', function (table) {
       table.increments('id');
       table.string('email',255).notNullable();
       table.string('name',255).notNullable();
       table.string('password',255).notNullable();
       table.text('usertoken');
       table.engine('InnoDB');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable("team");
};
exports.config = { transaction: false };