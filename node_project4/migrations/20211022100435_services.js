
exports.up = function(knex) {
    return knex.schema
    .createTable('services', function (table) {
       table.increments('id');
       table.string('service_name', 255).notNullable();
       table.engine('InnoDB');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable("services");
};
exports.config = { transaction: false };