
exports.up = function(knex) {
    return knex.schema
    .createTable('eb_resources', function (table) {
       table.increments('id');
       table.string('first_name',255).notNullable();
       table.string('last_name',255).notNullable();
       table.string('primary_skill',255).notNullable();
       table.string('vendor_name',255).notNullable();
       table.string('city',255).notNullable();
       table.decimal('cost_price',[10],[2]);
       table.decimal('sell_price',[10],[2]);
       table.string('total_experiance',255).notNullable();
       table.string('resume',255).notNullable();
       table.string('availability').notNullable();
       table.string('type',255).notNullable();
       table.timestamp('created_at').defaultTo(knex.fn.now());
       table.timestamp('updated_at',true);
       table.engine('InnoDB');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable("eb_resources");
};
exports.config = { transaction: false };