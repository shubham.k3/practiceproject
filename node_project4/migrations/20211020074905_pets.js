
exports.up = function(knex) {
    return knex.schema
    .createTable('pets', function (table) {
       table.increments('id');
       table.string('breed', 255).notNullable();
       table.string('name', 255).notNullable();
       table.integer('owner_id').notNullable();
       table.integer('gender').notNullable();
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable("pets");
};
exports.config = { transaction: false };