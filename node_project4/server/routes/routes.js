const express=require('express');
const route=express.Router();
const path=require('path');
const admin=require('../controller/Admincontroller');
const personObj=require('../controller/PersonController');
const petObj=require('../controller/PetController');
const validate_all=require('../validations/validation1');
const {body}=require('express-validator');
const middleware = require('../middleware/middleware');
const verifyToken = require('../middleware/verifyToken');
const verifyAdmin = require('../middleware/verifyAdmin');
const team=require('../controller/TeamController');
const { resourceLimits } = require('worker_threads');
const user = require('../controller/UserController');
const permissionCheck = require('../middleware/permissionCheck');
const resources = require('../controller/EbResourcesController');
const upload = require('../middleware/uploadImage');

const chatUserController = require('../controller/chatUserController');
const verifyChatUser = require('../middleware/verifyChatUser');

//pug pages
route.get('/login',admin.login);
route.post('/dologin',admin.dologin);

route.get('/signup',admin.signup);
route.post('/dosignup',admin.dosignup);
route.get('/showAllUsers',admin.showAllUsers);
route.get('/modify_adminpage/:id',admin.modify_adminpage);
route.get('/modify_admin',admin.modify_admin);
route.get('/remove_admin/:id',admin.remove_admin);

//token cases
route.post('/generateToken',admin.generateToken);
route.post('/refreshToken',admin.refreshToken);
route.get('/showTokenList',admin.showTokenList);

route.post('/login1',admin.login1);
route.post('/token',admin.refreshToken);
route.get('/secure',middleware,admin.secure);
route.get('/logout_blacklist',verifyToken,admin.logout_blacklist);

//apis
route.post('/addAdmin',admin.addAdmin);
route.get('/showAdmins',admin.showAdmins);
route.get('/show_a_admin/:id',admin.show_a_admin);
route.post('/modifyAdmin/:id',admin.modifyAdmin);
route.get('/verifyUserByToken',admin.verifyUserByToken);

//for objection testing
route.get('/addPerson',personObj.addPerson);//load page
route.post('/savePerson',personObj.savePerson);
route.get('/addPet',petObj.addPet);//load page
route.post('/savePet',petObj.savePet);

route.post('/person_login',personObj.person_login);//working fine
route.post('/add_single_person',personObj.add_single_person);//working fine
route.get('/get_a_person/:id',personObj.get_a_person);//working fine
route.get('/get_all_persons',personObj.get_all_persons);//working fine
route.get('/remove_person_by_id/:id',personObj.remove_person_by_id);//working fine
route.get('/remove_person_by_name/:name',personObj.remove_person_by_name);//working fine

route.post('/upload_single_image',admin.upload_single_image);
route.get('/multiple_img_page',admin.multiple_img_page);
route.post('/upload_mltiple_image',admin.upload_mltiple_image);
route.post('/enc_password',admin.enc_password);

route.get('/testing_join',personObj.testing_join);
route.post('/validationEx',body('username').isLength({
    min: 6
}),personObj.validationEx);

route.get('/send_mail',personObj.send_mail);
route.get('/template_view',personObj.template_view);

route.get('/sendgrid_template_view',personObj.sendgrid_template_view);
route.get('/sendgrid_mailsend_func',personObj.sendgrid_mailsend_func);
route.post('/sendgrid_mailsend_func1',personObj.sendgrid_mailsend_func1);


//token wise api creation
route.post('/logMeIn',admin.logMeIn);//create token and refresh token
route.post('/refreshMyToken',admin.refreshMyToken);//create new token using refresh token
route.get('/dashboard',verifyAdmin,admin.dashboard);// to see if token is working fine 
route.post('/reject_refreshToken',admin.reject_refreshToken);// to remove refresh token
route.get('/viewRefreshToken',admin.viewRefreshToken);// to see refresh token

route.get('/addBlacklistToken',admin.addBlacklistToken);// testing adding token to blacklist


//token new practice
route.post('/team_login',team.login_team);//validate user,create normal/refresh token
route.post('/refresh_team_token',team.refresh_team_token);//create new token with refresh token
route.get('/view_refresh_token',team.view_refresh_token);//to view refresh token
route.get('/homePage',verifyAdmin,team.homePage);//home page after authentication
route.post('/logoutmy_team',team.logout_team);//logout remove token

route.get('/runStrictFromBackend',team.runStrictFromBackend);
route.get('/deduct_money',team.deduct_money);//rajor pay trying
route.post('/verify_payment',team.verify_payment);
route.post('/paymentCapture',team.paymentCapture);

//user related apis
route.post('/loginUser',user.loginAsUser);

route.post('/checkUser',permissionCheck.adminAccess,user.checkUser);
route.get('/checkAdmin',user.checkAdmin);
route.get('/checkClient',user.checkClient);
route.get('/checkCustomer',user.checkCustomer);

//supersourcing crud
route.post('/save-developers',resources.saveDevelopers);//in middleware  upload.fields([{name: 'resume', maxCount:1}
route.get('/show-eb-resources',resources.showEbResources);
route.get('/remove-resource/:id',resources.remove_resource);
route.get('/get-a-resource/:id',resources.get_a_resource);
route.post('/modify_resource/:id',resources.modify_resource);

route.post('/upload_file_check',upload.uploadaImage,resources.upload_file_check);

//chat apis

    //admin side
route.get('/remove_user',chatUserController.remove_user);
route.get('/user_info',chatUserController.user_info);
route.get('/all_users',chatUserController.all_users);
route.get('/change_user_status',chatUserController.change_user_status);

    //client side
route.post('/login',chatUserController.login);
route.post('/register',chatUserController.register);
route.get('/get_user_info',verifyChatUser,chatUserController.get_user_info);
route.post('/update_user_profile',verifyChatUser,chatUserController.update_user_profile);
route.post('/logout_team',verifyChatUser,chatUserController.logout_team);
route.post('/reset_user_password',verifyChatUser,chatUserController.reset_user_password);
route.post('/forgot_user_password',chatUserController.forgot_user_password);

    //under working
route.get('/showFriendsList',chatUserController.showFriendsList);
route.get('/showNotFriends',chatUserController.showNotFriends);
route.get('/notifications_for_users',chatUserController.notifications_for_users);
route.get('/remove_user',chatUserController.remove_user);

route.get('/middlewareCheck',verifyChatUser,chatUserController.middlewareCheck);

module.exports=route;