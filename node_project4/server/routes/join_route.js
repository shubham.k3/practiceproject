const express=require('express');
const route=express.Router();
const companyObj=require('../controller/CompanyController');
const serviceObj=require('../controller/ServicesController');

route.get('/addCompany',companyObj.addCompany);
route.post('/saveCompany',companyObj.saveCompany);
route.get('/show_all_company',companyObj.show_all_company);

route.get('/addService',serviceObj.addService);
route.post('/saveService',serviceObj.saveService);
route.get('/show_all_services',serviceObj.show_all_services);

module.exports=route;