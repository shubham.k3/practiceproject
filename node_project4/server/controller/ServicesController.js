const Services=require('../models/Services');

exports.addService=(req,res)=>{
    res.render("./task/add_services");
    // res.send("add service");
}

exports.saveService=async (req,res)=>{
    req_data={
        service_name:req.body.service_name
    }
    service_rs=await Services.query()
    .select('*')
    .where('service_name',req.body.service_name);
    if(service_rs.length){
        res.send("service already exist");
    }else{
        Services.query()
        .insert(req_data)
        .then(result=>{
            if(result){
                res.send("service added successfully");
            }else{
                res.send("error in adding service");
            }
        })
        .catch(err=>{
            res.send("error occured"+err);
        })
    }
    
}

exports.show_all_services=async (req,res)=>{
    await Services.query()
    .then(result=>{
        res.render('./task/show_services',{all_services:result})
    })
    .catch(err=>{
        res.send("error occured"+err.data);
    });
}