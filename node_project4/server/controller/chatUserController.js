"use strict";

const conn=require('../conn/knex');
const bcrypt=require('bcrypt');
const jwt = require("jsonwebtoken");
const User = require('../models/UserModel');
const { token } = require('morgan');
const SECREAT = "secret hai";

exports.login = async(req,res) => {
    let data = req.body;
    if(!data.phone_no){
        res.status(404).json({"message":"Please fill phone no."});
    }
    if(!data.password){
        res.status(404).json({"message":"Please fill password."});
    }
    try{
        let phone_no = data.phone_no;
        let password = data.password;
        var user_info = await User.query()
            .select('*')
            .where('phone_no',phone_no)
            .first();
        if(!user_info){
            res.status(404).json({"message":"user not exist"});
        }else{
            let comparision=await bcrypt.compare(password, user_info.password);
            if(!comparision){
                res.status(404).json({"message":"password not matched"});  
            }else{
                let mytoken = jwt.sign({"user_id": user_info.id}, SECREAT, { expiresIn: '60s'})//5 min
                var user_data = await User.query()
                    .patch({"token":mytoken})
                    .where('phone_no',phone_no)
                    .first();
                delete user_info.token;
                delete user_info.password;
                let response ={
                    user_info
                }
                res.set('Authorization', "Bearer "+mytoken);
                res.status(200).json({"message":"logged in successfully","response":response});
            }                
        }
    }catch(error){
        res.status(404).json({"message":"error"});
    }
}

exports.register = async(req,res) => {
    let data = req.body;
    if(!data.mode){
        res.status(404).json({"message":"Please fill mode."});
    }
    
    try{
        if(data.mode==1){
            if(!data.phone_no){
                res.status(404).json({"message":"Please fill phone no."});
            }
            if(!data.password){
                res.status(404).json({"message":"Please fill password."});
            }
            let user_exist = await User.query()
            .select('id')
            .where('phone_no','=',data.phone_no)
            .first();
            if(user_exist){
                res.status(404).json({"message":"phone no already exist"});
            }else{
                delete data.mode; 
                let hashPass= await bcrypt.hash(data.password , 10);
                data.password = hashPass;
                let user_response = await User.query()
                    .insert(data);
                if(user_response){
                    res.status(200).json({"message":"user successfully registered"});
                }else{
                    res.status(404).json({"message":"some thing went wrong"});
                }
            }
        }else{
            res.status(404).json({"message":"invalid mode entered"});    
        }
    }catch(error){
        res.status(404).json({"message":"error"});
    }
}

//for client side
exports.get_user_info = async(req,res) => {
    try{
        let user_id = req.user_id;//get from auth
            let user_response = await User
            .query()
            .select("*")
            .where('id', "=", user_id);
        if(!user_response){
            res.status(404).json({"message":"user not exist"});
        }else{
            delete user_response[0].token;
            delete user_response[0].password;
            let responseData= {
                user_response
            }
            res.status(200).send(responseData);
        }
    }catch(error){
        res.status(404).json({"message":"error"});
    }

}

//for admin side
exports.user_info = async(req,res) => {
    try{
        if(!req.params.id){
            res.status(404).json({"message":"Id not  found."});
        }
        let id = base64decode(req.params.id);
            let user_response = await User
            .query()
            .select("*")
            .where('id', "=",id);
        if(!user_response){
            res.status(404).json({"message":"user not exist"});
        }else{
            let responseData= {
                user_response
            }
            res.status(200).send(responseData);
        }
    }catch(error){
        res.status(404).json({"message":"error"});
    }

}

//admin side
exports.all_users = async(req,res) => {
    try{
        let users_response = await User
        .query()
        .select("*")
        .orderBy('id');
        if(!user_response){
            res.status(404).json({"message":"user not exist"});
        }else{
            let responseData= {
                users_response
            }
            res.status(200).send(responseData);
        }
    }catch(error){
        res.status(404).json({"message":"error"});
    }

}

exports.update_user_profile = async (req,res) => {
    const data = req.body;
    if(data.first_name && data.first_name==''){
        res.status(404).json({"message":"Please fill first name."});
    }
    if(data.last_name && data.last_name==''){
        res.status(404).json({"message":"Please fill last name."});
    }
    if(data.email && data.email==''){
        res.status(404).json({"message":"Please fill email."});
    }
    if(data.user_name && data.user_name==''){
        res.status(404).json({"message":"Please fill user name."});
    }
    if(data.city && data.city==''){
        res.status(404).json({"message":"Please fill user city."});
    }
    if(data.state && data.state==''){
        res.status(404).json({"message":"Please fill user state."});
    }
    try{
        // const token=req.headers.authorization.split(" ")[1];
        let user_id = req.user_id;
        let user_response = await User.query()
            .patch(data)
            .where('id','=',user_id);
        if(user_response){
            res.status(200).json({"message":"profile updated successfully"});
        }else{
            res.status(404).json({"message":"some thing went wrong"});
        }
    }catch(error){
        res.status(404).json({"message":"error"});
    }
}

exports.showFriendsList = async (req,res) => {
    try{
        res.status(404).json({"message":"try"});
    }catch(error){
        res.status(404).json({"message":"error"});
    }
}

exports.showNotFriends = async (req,res) => {
    try{
        res.status(404).json({"message":"try"});
    }catch(error){
        res.status(404).json({"message":"error"});
    }
}

exports.logout_team = async (req,res) => {
    try{   
        let user_id = req.user_id;
        let modify_status = await User.query()
        .patch({token:null})
        .where('id', '=', user_id);
        if(!modify_status){
            res.status(404).json({"message":"error while logout"});
        }else{
            res.status(200).json({"message":"logged out successfully"});
        }        
    }catch(error){
        res.status(404).json({"message":"something went wrong"})
    }
}

exports.reset_user_password = async (req,res) => {
    /*let hashPass= await bcrypt.hash(data.password , 10);
    data.password = hashPass;*/
    let body= req.body;
    if(!body.current_password){
        res.status(404).send("Please fill current password");
    }
    if(!body.new_password){
        res.status(404).send("Please fill new password");
    }

    try{
        let current_password = req.body.password;
        let new_password = req.body.new_password;
        let user_id = req.user_id;
        let comparision=await bcrypt.compare(current_password ,new_password);
        if(comparision===true){
            let modify_status = User.query()
                .patch({ "password": new_password  })
                .where(id, '=', user_id);//get token from auth token
            res.status(200).json({"message":"password reset sucessfully"});
        }else{
            res.status(404).json({"message":"invalid password"});
        }
    }catch(error){
        res.status(404).json({"message":"error","error":error});
    }
}

exports.forgot_user_password = async (req,res) => {
    const body = req.body;
    if(!body.email){
        res.status(404).json({"message":"Please fill email id"});
    }
    try{
        let email = req.body.email;
        let verify_email = User.query()
            .select("id")
            .where("email", '=', email);
        if(!verify_email){
            res.status(404).json({"message":"email id not exist"});
        }else{
            var result           = '';
            var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for ( var i = 0; i < 8; i++ ) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            var new_password = result;
            let hashPass= await bcrypt.hash(new_password, 10); 
            let modify_status = User.query()
                .insert({ "password": hashPass, "token":null  })
                .where("email", '=', email);//get token from auth token
            res.status(200).json({"message":"password updated successfully","new _password":new_password});
        }
    }catch(error){
        res.status(200).json({"message":"something went wrong"});
    }
}

exports.notifications_for_users = async (req,res) => {
    
}

exports.change_user_status = async (req,res) => {
    let body= req.body;
    if(!body.status){
        res.status(404).send("User not found");
    } 
    try{    let status=body.status;
        //1=profile complete, 2=approved, 3=blocked, 4=profile deactivated
        let modify_status = await User.query()
            .patch({ "status": status  })
            .where(token, '=', 1);//get token from auth token
        if(!modify_status){
            res.status(404).json({"message":"error occured"});
        }else{
            res.status(200).json({"message":"status changed succesfully"});
        }
    }catch(error){
        res.status(404).json({"message":"error","error":error});
    }
}

exports.remove_user = async (req,res) => {
    try{
        let user_id = req.user_id;
        let delete_response = await User.query().deleteById(user_id);
        if(!delete_response){
            res.status(404).json({"message":"error occured"});
        }else{
            res.status(200).json({"message":"user successfully removed"});
        }
    }catch(error){
        res.status(404).json({"message":"error","error":error});
    }

}

exports.encript_password = async (req,callback) => {
    bcrypt.hash(req.body.password,10,function(err,hash){
        return callbackPromise(err,hash)
    });
}

exports.middlewareCheck = async (req,res) => {
    res.send("middleware function running");
}