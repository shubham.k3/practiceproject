const Company=require('../models/Company');
const Service=require('../models/Services');

exports.addCompany=async(req,res)=>{
    await Service.query()
    .then(result=>{
        if(result){
            res.render("./task/add_company",{services:result});
        }else{
            res.send("no services cannot add company")
        }
    })
    .catch(err=>{
        res.send("error occured "+err)
    })
}

exports.saveCompany=async (req,res)=>{
    //console.log(req.body);
    req_data={
        name:req.body.company_name,
        email:req.body.email,
        phone_no:req.body.phone_no,
        address:req.body.address
    }
    await Company.query()
    .insert(req_data)
    .then(result=>{
        if(result){
            console.log(req.body.service[0] );
            // console.log((req.body.service).length );
            id=result.id;
            selected_service_count=(req.body.service).length;
            for(i=0;i<selected_service_count;i++){
                Company.relatedQuery('services')
                .for(id)
                .relate(req.body.service[i])
                .then(result=>{
                    if(result){
                        res.send("company added and services attached successfully");
                    }else{
                        res.send("error while attaching services");
                    }
                })
                .catch(err=>{
                    res.send("error occured"+err);
                });
            }
            /*Company.relatedQuery('services')
            .for(id)
            .relate(req.body.service[i])
            .then(result=>{
                if(result){
                    res.send("company added and services attached successfully");
                }else{
                    res.send("error while attaching services");
                }
            })
            .catch(err=>{
                res.send("error occured"+err);
            });*/
            res.send("company added successfully");
        }else{
            res.send("error in adding company");
        }
    })
    .catch(err=>{
        res.send("error occured"+err);
    })
}

exports.show_all_company=async (req,res)=>{
    await Company.query()
    .then(result=>{
        res.render("./task/show_company",{companies:result})
        res.json(result);
    })
    .catch(err=>{
        res.send("error occured"+err.data);
    });
}