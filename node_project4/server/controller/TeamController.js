const conn=require('../conn/knex');
const bcrypt=require('bcrypt');
const jwt = require("jsonwebtoken");
const SECREAT="SECREAT_HAI";
const REFRESH_SECREAT="REFRESH_SECREAT_HAI";
// const validate=require('express-validator');
// const { render } = require('pug');
const shortid = require("shortid");
const gtts = require('gtts')
const crypto = require("crypto");

"use strict";


const team = require('../models/Team');
const refreshTokenList={}

const Razorpay = require('razorpay');
const razorpay_instance = new Razorpay({
    key_id:'rzp_test_MiXnJ01zMCHp5X',//rzp_test_uGoq5ABJztRAhk
    key_secret:'RlzQUtywNNMW4skC11VGIT3X'//FySe2f5fie9hij1a5s6clk9B
  }); 

exports.login_team =async  (req,res) => {
    const Body=req.body;
    let email=Body.email;
    let password=Body.password;
    team_member=await team.query()
    .select('*')
    .where('email',email)
    .where('password',password);

    if(team_member.length==1){

        let mytoken = jwt.sign({"team_member":team_member[0].id}, SECREAT, { expiresIn: '60s'})//1 min
        let refresh_token = jwt.sign({"team_member":team_member[0].id}, REFRESH_SECREAT, { expiresIn: '60s'})//1 min
        // console.log(mytoken);
        //add token in db

        let tokenUpdated_res=await team.query()
        .patch({ usertoken : mytoken })
        .where('id','=', team_member[0].id );

        const response={
            "status":200,
            "message":"logged in successfully",
            "token":mytoken,
            "refresh_token":refresh_token,
        }
        refreshTokenList['refreshToken']=refresh_token;
        res.set('Authorization', mytoken);

        res.status(200).json(response);
    }else{
        res.status(404).json({
            "status":404,
            "message":"team member not exist"});
    }
}

exports.refresh_team_token =async  (req,res) => {
    const body=req.body;
    if(!body.email){
        res.status(404).json({"message":"email not found"});
    }
    else if(!body.refresh_token){
        res.status(404).json({"message":"refresh token not found"});
    }
    else if( refreshTokenList['refreshToken']===undefined){
        res.status(404).json({"message":"refresh token list is empty"});
    }
    else if(refreshTokenList['refreshToken'] !=body.refresh_token){
        res.status(404).json({"message":"refresh token not matched"});
    }
    else{
        var email=body.email;
        var refresh_token=body.refresh_token;   
        let mytoken = jwt.sign({"team_member":team_member}, SECREAT, { expiresIn: '60s'})//1 min
            let tokenUpdated_res=await team.query()
            .patch({ usertoken : mytoken })
            .where('email','=', email );
            res.status(200).json({
                "message":"token reseted",
                "token":mytoken
            });
    }
}

exports.view_refresh_token =async  (req,res) => {
    var status;
    let response={}
    if(refreshTokenList.length===0){
        status=200
        response={
            "status":200,
            "message":"refresh token exist",
            "refreshToken":refreshTokenList
        }
    }else{
        status=404
        response={
            "status":404,
            "message":"no refresh token exist",
            "refreshToken":refreshTokenList
        }
    }
    res.status(status).json(response);
}

exports.homePage = async (req,res) => {
    res.send("home page")
}

exports.logout_team = async (req,res) => {
    const token =  req.headers['authorization'];
    if(1==1){//token
        const tokenRemovedRes = await team.query()
        .patch({ usertoken : null })
        .where('usertoken', '=', token);
        if(tokenRemovedRes){
            res.status(200).json({"message":"logged out successfully"});
        }else{
            res.status(500).json({"message":"team member already logged out"});
        }
    }else{
        res.status(404).json({"message":"Token not found"});
    }
}

exports.runStrictFromBackend = async (req,res) => {
    let x="<script>alert('hello world')</script>";
    res.send(x);
}

// razor pay payment gateway 
exports.deduct_money = async (req,res) => {
    const options = {
        amount: 101,
        currency: "INR",
        receipt: shortid.generate(), //any unique id
    }
    try{
        razorpay_instance.orders.create(options,function(err,resp){
            console.log(err);
            res.send(resp);
        });
    }catch(error){
        res.status(404).json({"message":"unable to create order"});
    }
}

exports.verify_payment = async (req,res) => {
    let body=req.body.razorpay_order_id+ "|" + req.body.razorpay_payment_id;
    var expectedSignature = crypto.createHmca('sha256','asdf')
        .update(body.toString())
        .digest('hex');
        console.log("sig recieved ",req.body.response.razorpay_signature);
        console.log("sig generated ",expectedSignature);
    var response = {"signature":"false"};
    if(expectedSignature===req.body.response.razorpay_signature){
        response= {"signature":"true"};
        res.send(response);
    }
}

const secret_key = '12345678'
exports.paymentCapture = (req, res) => {
    // do a validation
    const data = crypto.createHmac('sha256', secret_key)
    data.update(JSON.stringify("hello"))//req.body
    const digest = data.digest('hex')
    if (digest === req.headers['x-razorpay-signature']) {
        console.log('request is legit')
        //we can store detail in db and send the response
        res.json({
            status: 'ok'
        })
    } else {
        res.status(400).send('Invalid signature');
    }
}