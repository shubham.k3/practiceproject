const conn=require('../conn/knex');
const bcrypt=require('bcrypt');
const jwt = require("jsonwebtoken");
const SECRET="SECREAT_HAI";
const validate=require('express-validator');
const { render } = require('pug');
const multer=require('multer');
const image_path=multer({dest:'/assets/images'});
const path=require('path');
const { body } = require('express-validator');

const LOGIN_SECRET1="secret";
const REFRESH_SECRET="refresh secret";

"use strict";

const person = require('../models/Person');


const imageStorage =multer.diskStorage({
    destination : 'assets/images',
    filename:(req,file,cb)=>{
        img_name=file.fieldname + '-' + Date.now() + path.extname(file.originalname);
        console.log(file.fieldname + '-' + Date.now() + path.extname(file.originalname));
        cb(null,file.fieldname+'-'+Date.now()+path.extname(file.originalname))
    }
});

const imageUpload=multer({
    storage:imageStorage,
    limit:{fileSize:15000000},// 15 MB
    fileFilter(req,file,cb){
        if(!file.originalname.match(/\.(png|jpg|jpeg|svg)$/)){
            return cb(new Error('please upload image'))
        }
        cb(undefined,true)
    }
}).single('profile_image');

const imageMultiUpload=multer({
    storage:imageStorage,
    limit:{fileSize:15000000},// 15 MB
    fileFilter(req,file,cb){
        if(!file.originalname.match(/\.(png|jpg|jpeg|svg)$/)){
            return cb(new Error('please upload image'))
        }
        cb(undefined,true)
    }
}).single('profile_image');

//login signup flow

exports.login=(req,res)=>{
    res.render("login");
}
exports.dologin=(req,res)=>{
    email=req.body.email;
    conn('admin').where('email',email).select('*')
    .then(function(result){
        if(!result){
            res.json({"message":"User not exist"});
        }else{
            bcrypt.compare(req.body.password,result[0].password).then(isAuthenticated=>{
                if(!isAuthenticated){
                    res.json({"message":"User not Authorized"});
                }else{
                    jwt.sign({result:result[0]},SECRET,{expiresIn : "60s" },(error,token)=>{
                        // res.send(200).json("token");
                        res.json({"message":"user verified","token":token});
                     })
                }
            });
        }
    }).catch(err=>{
        res.status(500).send({
            message:err.message || "some error occured"
        })
    });
}

exports.verifyUserByToken=(req,res)=>{
    const token=req.headers.authorization.split(" ")[1];
    jwt.verify(token,SECRET,(error,decodedToken)=>{
        if(error){
            res.status(401).json({"message":"Unauthorized access"});
        }else{
            res.status(200).json({"data":decodedToken});
        }
    });
    res.send("verify user");
}

exports.signup=(req,res)=>{
    res.render("signup");
    // res.send("signup page");
}

exports.dosignup=(req,res)=>{
    
    bcrypt.hash(req.body.password,10).then(hashPass=>{
        imageUpload(req, res, function (err) {
            if (err instanceof multer.MulterError) {
               res.send(err);
            } else if (err) {
               res.send(err);
            }else{
                res.send("uploaded");
                req_data={
                    username:req.body.username,
                    email:req.body.email,
                    password:hashPass,
                    role:req.body.role
                }
                // res.send("bhago");
                conn('admin').insert(req_data)
                .then(function(result){
                    // res.render("dashboard");
                    res.redirect('showAllUsers');
                }).catch(err=>{
                    res.status(500).send({
                        message:err.message || "some error occured"
                    })
                });
            }
        });
        
    });
    // res.send("run");
}

exports.showAllUsers=(req,res)=>{
    conn.select('*').from('admin').then(function(result){
        // console.log(result);        
        res.render('dashboard',{admin:result});
    });
}

exports.modify_adminpage=(req,res)=>{
    id=req.params.id;
    conn('admin').where('id',id).select('*')
        .then(function(result){
            res.render("update",{data:result});
        }).catch(err=>{
            res.status(500).send({
            message:err.message || "some error occured"
        })
    }); 
}

exports.modify_admin=(req,res)=>{
    res.send("update");
}

exports.remove_admin=(req,res)=>{
    id=req.params.id;
    conn('admin').where('id',id).del()
        .then(function(result){
            res.redirect("/showAllUsers");
        }).catch(err=>{
            res.status(500).send({
            message:err.message || "some error occured"
        })
    });     
}

//show all admin
exports.showAdmins=(req,res)=>{
    conn.select('*').from('admin')
    .then(function(result){
        res.send(result);
    }).catch(err=>{
        res.status(500).send({
            message:err.message || "some error occured"
        })
    });
}

//remove a admin
exports.removeAdmin=(req,res)=>{
    id=req.body.id;
    conn('admin').where('id',id).del()
        .then(function(result){
            res.send("data removed succesfully");
        }).catch(err=>{
            res.status(500).send({
                message:err.message || "some error occured"
            })
        });     
}

//update admin
exports.modifyAdmin=(req,res)=>{
    id=req.params.id;
    req_data={
        username:req.body.username,
        email:req.body.email
    }
    conn('admin')
        .where('id','=',id)
        .update(req_data)
        .then(function(result){
            res.send("data updated successfully");
        }).catch(err=>{
            res.status(500).send({
                message:err.message || "some error occured"
            })
        })
}

//add admin
exports.addAdmin=(req,res)=>{
    
    bcrypt.hash(req.body.password,10).then(hashPass=>{
        req_data={
            username:req.body.username,
            email:req.body.email,
            password:hashPass,
            role:req.body.role
        }
        conn('admin').insert(req_data)
        .then(function(result){
            res.send("data inserted succesfully");
        }).catch(err=>{
            res.status(500).send({
                message:err.message || "some error occured"
            })
        });
        
    });
        
}

//display single admin
exports.show_a_admin=(req,res)=>{
    id=req.params.id;
    conn.select('*').where('id',id).from('admin')
    .then(function(result){
        res.send(result);
    }).catch(err=>{
        res.status(500).send({
            message:err.message || "some error occured"
        })
    });
}


//upload single image
exports.upload_single_image=(req,res)=>{
    

    imageUpload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
           res.send(err);
        } else if (err) {
           res.send(err);
        }else{
            res.send("uploaded");
        }
    });

}


exports.enc_password=(req,res)=>{
    data=req.body;
    
    if(!data.password){
        console.log("password required");
    }else if(data.password.length<=5){
        console.log("password should be more than 5 char ");
    }else{

    }
    bcrypt.hash(req.body.password,10).then(hashPass=>{
        console.log(hashPass);
    });
    res.send("running");
   
}

exports.multiple_img_page=(req,res)=>{
    res.render('multi_img');
}
var upload = multer({ storage: imageStorage })
//upload multiple image
exports.upload_mltiple_image=upload.array('profile_image'),(req,res)=>{

    imageMultiUpload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
           res.send(err);
        } else if (err) {
           res.send(err);
        }else{
            res.send("uploaded");
        }
    });

}

const tokenList = {}

exports.generateToken = (req,res) => {
    const token=jwt.sign({result:'shubham'},SECRET,{expiresIn : "60s" }); // 2min
    const refreshToken=jwt.sign({result:'shiv'},"SECRET",{expiresIn : "120s" });
    const response={
        "status":"Logged In",
        "token": token,
        "refreshToken": refreshToken
    };
    tokenList['refreshToken']=response
    res.status(200).json({"message":"token created successfully","response" : response});
}

exports.showTokenList = (req,res) => {
    res.send(tokenList['refreshToken']);
}

exports.refreshToken = (req,res) => {
    const body=req.body;
    // console.log("token list is ");
    // console.log(tokenList['refreshToken']['refreshToken']);
    // console.log(body.refreshToken);
    if(body.refreshToken in tokenList){
        console.log("exist");
    }else{
        console.log("token not exist");
    }
    res.send("run");
    /*if((body.refreshToken) && (body.refreshToken in tokenList)){
        const token=jwt.sign({result:'shubham'},SECRET,{expiresIn : "120s" }); // 2min
        const repsonse = {
            "token" : token,
        }
        tokenList[body.refreshToken].token=token;
        res.send(200).send(response);
    }else{
        res.json({"message":"invalid request"});
    }*/
}

exports.login1= (req,res) => {
    const postData = req.body;
    const user = {
        "email": postData.email,
        "name": postData.name
    }
    // do the database authentication here, with user name and password combination.
    const token = jwt.sign(user, "shub1", { expiresIn: '30s'})
    const refreshToken = jwt.sign(user, "shub2", { expiresIn: '120s'})
    const response = {
        "status": "Logged in",
        "token": token,
        "refreshToken": refreshToken,
    }
    tokenList[refreshToken] = response
    res.cookie('Authorization',token);
    res.status(200).json(response);
}

exports.refreshToken =  (req,res) => {
    // refresh the damn token
    const postData = req.body
    // if refresh token exists
    const req_data = {
        "email": postData.email,
        "name": postData.name,
        "refreshToken":postData.refreshToken
    }
    // console.log(req_data);
    if((postData.refreshToken) && (postData.refreshToken in tokenList)) {
        const user = {
            "email": postData.email,
            "name": postData.name
        }
        // console.log("running");
        const token = jwt.sign(user, 'shub1', { expiresIn: '120s'})
        const response = {
            "token": token,
        }
        // update the token in the list
        tokenList[postData.refreshToken].token = token
        res.status(200).json(response);        
    } else {
        res.status(404).send('Invalid request')
    }
}

exports.secure=  (req,res) => {
    // all secured routes goes here
    res.send('I am secured...')
}

///token wise new flow
var newTokenList={};

//login
exports.logMeIn =async (req,res)=>{
    const body=req.body;
    if(!body.email){
        // throw new Error("email is required");
        res.status(404).send("email is required");
    }
    try{
        const personData=await person.query().select('*').where({'email':body.email});
        if(personData){
            // res.status(200).json(personData);
            let token = jwt.sign({"personData":personData}, LOGIN_SECRET1, { expiresIn: '60s'})//1 min
            let refresh_token = jwt.sign({"personData":personData}, REFRESH_SECRET, { expiresIn: '60s'})//2 min
            const response = {
                "status": "Logged in successfully",
                "personData":personData,
                "token": token,
                "refreshToken": refresh_token,
            }
            newTokenList['refreshToken'] = refresh_token;
            res.set('Authorization', token);
            res.status(200).json(response);
        }else{
            res.status(200).send("no person found");
        }
    }catch(err){
        res.send("error occured: "+err);
    }
    
}

exports.refreshMyToken = async (req,res) => {
    const body=req.body;
    if(!body.email){
        res.status(404).send("email is required");
    }
    if(!body.refreshToken){
        res.status(404).send("refresh token  is required");
    }
    try{
        // console.log(newTokenList);
        // console.log(newTokenList['refreshToken']['refreshToken']);
        if((body.refreshToken) && newTokenList['refreshToken']['refreshToken'] ){
            const personData=await person.query().select('*').where({'email':body.email});
            let token = jwt.sign({"personData":personData}, LOGIN_SECRET1, { expiresIn: '60s'})//1 min
            newTokenList[refreshToken] = token;
            let response={
                "status": "Already Logged in",
                "msg":"token refreshed successfully",
                "personData":personData,
                "token": token
            }
            res.status(200).json(response);
        }else{
            res.status(404).send("token not exist");
        }
    }catch(err){
        res.send("error occured"+err);
    }
}

exports.dashboard = (req,res) =>{
    if(!req.headers['Authorization']){
        res.send("header not found");
    }
        res.send("welcome to dashboard, user also verified");
}

exports.reject_refreshToken = (req,res) => {
    const body=req.body;
    var refreshToken=body.refreshToken
    // console.log(newTokenList)
    if(refreshToken == newTokenList['refreshToken']){
        let personData=conn.select('*')
        .where({'email':'shub@gmail.com'})
        .from('persons')
        .then(function(result){
            console.log(result);
        })
        .catch(err=>{
            res.status(500).send({
                message:err.message || "some error occured"
            })
        });

        delete newTokenList['refreshToken'];
        res.send("refresh token found and deleted successfully");
        // res.send(newTokenList[refreshToken]);
    }else{
        res.send("refresh token not present");
    }
}

exports.viewRefreshToken = (req,res) => {
    res.send(newTokenList);
}

exports.logout_blacklist = (req,res) => {
    //verify and remove refresh token
    const body=req.body;
    const user_info= {
        user_email:body.user_email,
        token:body.token
    }   
    conn.select('*').where('email',body.user_email).from('persons')
    .then(function(result){
        if(result){
            res.send("a");
        }else{
            res.send("no user found");
        }
    }).catch(err=>{
        res.status(500).send({
            message:err.message || "some error occured"
        })
    });
    // res.send("log me out");
}


//testing of add tokens to blacklist list
exports.addBlacklistToken = (req,res) => {
    var b_token ='[d]';
    conn('person_logs').update({
        invalid_tokens: conn.raw(`jsonb_set(??, '{lat}', ?)`, ['jsonbColumn', 'newLatValue'])
    })
    .where({id:1}).then(function(){
        res.send("done");
    });
} 


