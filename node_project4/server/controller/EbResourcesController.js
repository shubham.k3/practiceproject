const { base64encode, base64decode } = require('nodejs-base64');
const Eb_resources = require('../models/Eb_resources');
const { transaction, ref, raw } = require("objection");


//image upload code
const multer=require('multer');
const image_path=multer({dest:'/assets/images'});
const path=require('path');
const imageStorage =multer.diskStorage({
    destination : 'assets/images',
    filename:(req,file,cb)=>{
        img_name=file.fieldname + '-' + Date.now() + path.extname(file.originalname);
        console.log(file.fieldname + '-' + Date.now() + path.extname(file.originalname));
        cb(null,file.fieldname+'-'+Date.now()+path.extname(file.originalname))
    }
});
const imageUpload=multer({
    storage:imageStorage,
    limit:{fileSize:15000000},// 15 MB
    fileFilter(req,file,cb){
        if(!file.originalname.match(/\.(png|jpg|jpeg|svg)$/)){
            return cb(new Error('please upload image'))
        }
        cb(undefined,true)
    }
}).single('profile_image');
//image upload code end

exports.saveDevelopers = async (req,res) => {
    data=req.body;
    if(!data.first_name){
        res.status(404).json({"message":"Please fill vender first name."});
        // throw globalCalls.badRequestError("Please fill vender first name.");
    }else if (data.first_name.length<=0 || data.first_name.length>=30){
        res.status(404).json({"message":"Vendor first name should be 30 characters"});
        // throw globalCalls.badRequestError("Vendor first name should be 30 characters");
    }

    if(!data.last_name){
        // throw globalCalls.badRequestError("Please fill vender last name.");
        res.status(404).json({"message":"Please fill vender last name."});
    }else if (data.last_name.length<=0 || data.last_name.length>=30){
        // throw globalCalls.badRequestError("Vendor last name should be 30 characters.");
        res.status(404).json({"message":"Vendor last name should be 30 characters."});
    }

    if(!data.primary_skill){
        // throw globalCalls.badRequestError("Please fill primary skill.");
        res.status(404).json({"message":"Please fill primary skill."});
    }

    if(data.type == "Vendor" && !data.vendor_name){
        // throw globalCalls.badRequestError("Please fill vendor name.");
        res.status(404).json({"message":"Please fill vendor name."});
    }else if (data.type == "Vendor" && (data.vendor_name.length<=0 || data.vendor_name.length>=30) ){
        res.status(404).json({"message":"Vendor name should be 30 characters."});
        // throw globalCalls.badRequestError("Vendor name should be 30 characters.");
    }
    
    if(!data.city){
        // throw globalCalls.badRequestError("Please fill city name.");
        res.status(404).json({"message":"Please fill city name."});
    }else if (data.city.length<=0 || data.city.length>=30){
        // throw globalCalls.badRequestError("City name should be of 30 characters.");
        res.status(404).json({"message":"City name should be of 30 characters."});
    }

    if(!data.cost_price){
        // throw globalCalls.badRequestError("Please fill cost price.");
        res.status(404).json({"message":"Please fill cost price."});
    }else if (isNaN(data.cost_price)){
        // throw globalCalls.badRequestError("Error! You have passed invalid cost price.");
        res.status(404).json({"message":"Error! You have passed invalid cost price."});
    }

    if(!data.sell_price){
        // throw globalCalls.badRequestError("Please fill sell price.");
        res.status(404).json({"message":"Please fill sell price."});
    }else if (isNaN(data.sell_price)){
        // throw globalCalls.badRequestError("Error! You have passed invalid sell price.");
        res.status(404).json({"message":"Error! You have passed invalid sell price."});
    }

    if(!data.total_experiance){
        // throw globalCalls.badRequestError("Please fill total experiance.");
        res.status(404).json({"message":"Please fill total experiance."});
    }else if (data.total_experiance.length>=30){
        // throw globalCalls.badRequestError("Total experiance should of 30 characters.");
        res.status(404).json({"message":"Total experiance should of 30 characters."});
    }

    if(!data.resume){
        // throw globalCalls.badRequestError("Please upload resume.");
        res.status(404).json({"message":"Please upload resume."});
    }

    /*
    if(req.files)
        {
            if (req.files.resume) {
                data.resume = req.files.resume[0].location;
            }
            delete data['resume'];
            
            if (req.files.resume) {
                data.resume = req.files.resume[0].location;
            }
            delete data['resume'];
        }
    */

    if(!data.availability){
        // throw globalCalls.badRequestError("Please select availability.");
        res.status(404).json({"message":"Please select availability."});
    }
    else if( data.availability != "Available" && data.availability != "Not Available" ){
        res.status(404).json({"message":"Error! You have choosed invalid availability."});
        // throw globalCalls.badRequestError("Error! You have choosed invalid availability.");
    }
    
    if(!data.type){
        // throw globalCalls.badRequestError("Please select type");
        res.status(404).json({"message":"Please select type"});
    }else if( data.type != "Vendor" && data.type != "Inhouse" ){
        res.status(404).json({"message":"Error! You have choosed invalid type."});
        // throw globalCalls.badRequestError("Error! You have choosed invalid type.");
    }

    try{
        // console.log(data);
        let resource_response = await Eb_resources.query()
        .insert(data);
        if(resource_response){
            res.status(200).json({"message":"inserted successfully"});
            //return globalCalls.okResponse(res, resource_response)
        }else{
            res.status(404).json({"message":"some thing went wrong while inserting"});
            // throw globalCalls.badRequestError("some thing went wrong");
        }
        // res.send("save function running");
    }catch(error){
        res.status(404).json({"message":"Something went wrong else","error":error});
        // throw globalCalls.badRequestError("Something went wrong else");
    }
}

exports.showEbResources = async (req,res) => {
    const per_page=10;
    let page = req.query.page ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : per_page;
    let offset = req.query.offset ? req.query.offset : limit * (page - 1);
    let resource_response = await Eb_resources
    .query()
    .select("*")
    .where((builder) => {
        if (req.query.search) {
            builder.andWhere(raw("concat(first_name, ' ', last_name)"), "like", req.query.search+"%");
        }
        if (req.query.primary_skill) {
            builder.andWhere("primary_skill", req.query.primary_skill);
        }
        if (req.query.type && (req.query.type=="Vendor" || req.query.type=="Inhouse" )) {
            builder.andWhere("type", req.query.type);
        }
        if (req.query.availability && (req.query.availability=="available" || req.query.availability=="notAvailable")) {
            builder.andWhere("availability", req.query.availability);
        }
        if (req.query.total_experiance) {
            builder.andWhere("total_experiance", req.query.total_experiance);
        }
    })
    .orderBy("id", "ASC")
    .offset(offset)
    .limit(limit);

    if(resource_response.length==0){
        res.status(404).json({"message":"no resource found"});
        // throw globalCalls.badRequestError("no resource found");
    }else{
        let responseData= {
            resource_response
        }
        res.status(200).send(responseData);
        // return globalCalls.okResponse(res, responseData, "");
    }

}

exports.get_a_resource = async (req,res) => {
    try{
        if(!req.params.id){
            res.status(404).json({"message":"No id found."});
            // throw globalCalls.badRequestError("No id found.");
        }else{
            let id = base64decode(req.params.id);
            let resource_response = await Eb_resources
            .query()
            .select("*")
            .where('id', "=",id);
            if(resource_response.length==0){
                res.status(404).json({"message":"No resource found"});
                // throw globalCalls.badRequestError("No resource found");
            }else{
                let responseData= {
                    resource_response
                }
                res.status(200).send(responseData);
                // return globalCalls.okResponse(res, responseData, "");
            }
        }
    }catch(error){
        res.status(404).json({"message":"something went wrong"});
        // throw globalCalls.badRequestError("something went wrong");
    }
}

exports.remove_resource= async (req,res)=> {
    try{
        if(req.params.id){
            let id = base64decode(req.params.id);
            const deleted_response = await Eb_resources.query()
            .delete()
            .where('id','=',id);
            if(deleted_response){
                res.status(200).json({"message":"Resource removed sucessfully."});
                // throw globalCalls.badRequestError("Resource removed sucessfully.");
            }else{
                // throw globalCalls.badRequestError("something went wrong.");
                res.status(404).json({"message":"something went wrong."});    
            }
        }else{
            res.status(404).json({"message":"Error! You have passed invalid-id."});
            // throw globalCalls.badRequestError("Error! You have passed invalid-id.");
        }
    }catch(error){
        // throw globalCalls.badRequestError("something went wrong");
        res.status(404).json({"message":"something went wrong."});       
    }
}

exports.modify_resource =async (req,res)=> {
    try{
        if(req.params.id){
            let id = base64decode(req.params.id);
            data=req.body;
            if(!data.first_name){
                res.status(404).json({"message":"Please fill vender first name."});
                // throw globalCalls.badRequestError("Please fill vender first name.");
            }else if (data.first_name.length<=0 || data.first_name.length>=30){
                res.status(404).json({"message":"Vendor first name should be 30 characters"});
                // throw globalCalls.badRequestError("Vendor first name should be 30 characters");
            }
        
            if(!data.last_name){
                // throw globalCalls.badRequestError("Please fill vender last name.");
                res.status(404).json({"message":"Please fill vender last name."});
            }else if (data.last_name.length<=0 || data.last_name.length>=30){
                // throw globalCalls.badRequestError("Vendor last name should be 30 characters.");
                res.status(404).json({"message":"Vendor last name should be 30 characters."});
            }

            if(!data.primary_skill){
                // throw globalCalls.badRequestError("Please fill primary skill.");
                res.status(404).json({"message":"Please fill primary skill."});
            }
        
            if(data.type == "Vendor" && !data.vendor_name){
                // throw globalCalls.badRequestError("Please fill vendor name.");
                res.status(404).json({"message":"Please fill vendor name."});
            }else if (data.type == "Vendor" && (data.vendor_name.length<=0 || data.vendor_name.length>=30) ){
                res.status(404).json({"message":"Vendor name should be 30 characters."});
                // throw globalCalls.badRequestError("Vendor name should be 30 characters.");
            }
            
            if(!data.city){
                // throw globalCalls.badRequestError("Please fill city name.");
                res.status(404).json({"message":"Please fill city name."});
            }else if (data.city.length<=0 || data.city.length>=30){
                // throw globalCalls.badRequestError("City name should be of 30 characters.");
                res.status(404).json({"message":"City name should be of 30 characters."});
            }
        
            if(!data.cost_price){
                // throw globalCalls.badRequestError("Please fill cost price.");
                res.status(404).json({"message":"Please fill cost price."});
            }else if (isNaN(data.cost_price)){
                // throw globalCalls.badRequestError("Error! You have passed invalid cost price.");
                res.status(404).json({"message":"Error! You have passed invalid cost price."});
            }
        
            if(!data.sell_price){
                // throw globalCalls.badRequestError("Please fill sell price.");
                res.status(404).json({"message":"Please fill sell price."});
            }else if (isNaN(data.sell_price)){
                // throw globalCalls.badRequestError("Error! You have passed invalid sell price.");
                res.status(404).json({"message":"Error! You have passed invalid sell price."});
            }
        
            if(!data.total_experiance){
                // throw globalCalls.badRequestError("Please fill total experiance.");
                res.status(404).json({"message":"Please fill total experiance."});
            }else if (data.total_experiance.length>=30){
                // throw globalCalls.badRequestError("Total experiance should of 30 characters.");
                res.status(404).json({"message":"Total experiance should of 30 characters."});
            }
        
            if(!data.resume){
                // throw globalCalls.badRequestError("Please upload resume.");
                res.status(404).json({"message":"Please upload resume."});
            }
        
            if(!data.availability){
                // throw globalCalls.badRequestError("Please select availability.");
                res.status(404).json({"message":"Please select availability."});
            }
            else if( data.availability != "Available" && data.availability != "Not Available" ){
                res.status(404).json({"message":"Error! You have choosed invalid availability."});
                // throw globalCalls.badRequestError("Error! You have choosed invalid availability.");
            }
            
            if(!data.type){
                // throw globalCalls.badRequestError("Please select type");
                res.status(404).json({"message":"Please select type"});
            }else if( data.type != "Vendor" && data.type != "Inhouse" ){
                res.status(404).json({"message":"Error! You have choosed invalid type."});
                // throw globalCalls.badRequestError("Error! You have choosed invalid type.");
            }

            let resource_response = await Eb_resources.query()
            .findById(id)
            .patch(data)
            .returning("*");
            if(resource_response){
                res.status(404).json({"message":"resource updated successfully"});
                //return globalCalls.okResponse(res, resource_response)
            }else{
                res.status(404).json({"message":"something went wrong."});
                // throw globalCalls.badRequestError("some thing went wrong.");
            }
        }else{
            res.status(404).json({"message":"No id found."});
            // throw globalCalls.badRequestError("No id found.");
        }
    }catch(error){
        // throw globalCalls.badRequestError("something went wrong");
        res.status(404).json({"message":"something went wrong"});
    }
}

exports.upload_file_check = async (req,res,next) => {
    /*// res.setHeader('Content-Type', 'multipart/form-data');
    if(req.files){
        console.log("file being uploaded");
    }
    res.send("file is "+ req.files)
    // res.send(req.body.first_name);
    // res.send(req.body.message);
    // next();
    // res.send("func running, message is ");*/
    if(!req.files){
        console.log("file not selected");
    }else{
        console.log("file selected");
    }
    if(!req.files) {
        res.send({
            status: false,
            message: 'No file uploaded'
        });
    }
    console.log(req.body.message);
    /*imageUpload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
           res.send(err);
        } else if (err) {
           res.send(err);
        }else{
            res.send("uploaded"+req.body.message);
        }
    });*/
    
}