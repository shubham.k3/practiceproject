const conn=require('../conn/knex');//remove later
const bcrypt=require('bcrypt');
const jwt = require("jsonwebtoken");
const Person=require('../models/Person');
const Pet=require('../models/Pet');
const SECRET="SECREAT_HAI";
const {expressValidator }=require('express-validator');
const { body, validationResult } = require('express-validator');


// include nodemailer
const nodemailer = require('nodemailer');
var fs = require('fs');
const path=require('path');
const pug = require('pug');

const validator = (body, rules, customMessages, callback) => {
    const validation = new Validator(body, rules, customMessages);
    validation.passes(() => callback(null, true));
    validation.fails(() => callback(validation.errors, false));
};

exports.person_login=(req,res)=>{
    email=req.body.email;
    Person.query()
    .select('*')
    .where('email',email)
    .then(result => {
        if(result){
            // res.json(result);
            bcrypt.compare(req.body.password,result[0].password).then(isAuthenticated=>{
                if(!isAuthenticated){
                    res.json({"message":"Person not Authorized"});
                }else{
                    jwt.sign({result:result[0]},SECRET,{expiresIn : "120s" },(error,token)=>{
                        // res.send(200).json("token");
                        res.json({"message":"user verified","token":token});
                     })
                }
            });
        }else{
            res.json({"message":"person not found"});
        }
    }).catch(err=>{
        res.status(500).send({
            message:err.message || "some error occured"
        })
    });
}

exports.addPerson=(req,res)=>{
    // res.send("add person page");
    /*Person.query()
    .then(users => {
        res.json(users)
    })*/
    res.render("person");
}

exports.savePerson=(req,res)=>{
    bcrypt.hash(req.body.password,10).then(hashPass=>{
        req_data={
            name:req.body.name,
            email:req.body.email,
            password:hashPass,
            gender:req.body.gender
        }
        conn('persons').insert(req_data)
        .then(function(result){
            res.send("person added successfully");
        }).catch(err=>{
            res.status(500).send({
                message:err.message || "some error occured"
            });
        });
    });
}

//get single
exports.get_a_person=(req,res)=>{
    const token=req.headers.authorization.split(" ")[1];
    jwt.verify(token,SECRET,(error,decodedToken)=>{
        if(error){
            res.status(401).json({"message":"Unauthorized access"});
        }else{
            id=req.params.id;
            Person.query().findById(id)
            .then(result => {
                if(result){
                    res.json(result);
                }else{
                    res.json({"message":"no person found"});
                }
            });
        }
    });
}

//get all persons
exports.get_all_persons=(req,res)=>{
    const token=req.headers.authorization.split(" ")[1];
    jwt.verify(token,SECRET,(error,decodedToken)=>{
        if(error){
            res.status(401).json({"message":"Unauthorized access"});
        }else{
            Person.query()
            .then(result=>{
                // res.json(result);
                if(result){
                    res.json(result);
                }else{
                    res.json({"message":"no record found"});
                }
            });
        }
    });
}

exports.remove_person_by_id=(req,res)=>{
    const token=req.headers.authorization.split(" ")[1];
    jwt.verify(token,SECRET,(error,decodedToken)=>{
        if(error){
            res.status(401).json({"message":"Unauthorized access"});
        }else{
            // res.status(200).json({"data":decodedToken});
            Person.query()
            .deleteById(req.params.id)
            .then(result=>{
                if(result){
                    // res.json(result);
                    res.json({"message":"person removed successfully"});
                }else{
                    res.json({"message":"person not exist"});
                }
            });
        }
    });
}

exports.remove_person_by_name=(req,res)=>{
    naam=req.params.name;
    Person.query()
    .delete()
    .where("name",naam)
    .then(result=>{
        if(result){
            res.json(result);
        }else{
            res.json({"message":"person not exist"});
        }
    });
}

//add api(post)
exports.add_single_person=(req,res)=>{
    try{
        // err.data.name;
        // req.check("name","please enter name").isLength({"min":4})
        bcrypt.hash(req.body.password,10).then(hashPass=>{
            req_data={
                name:req.body.name,
                email:req.body.email,
                password:hashPass,
                gender:req.body.gender
            }
            Person.query()
            .insert(req_data)
            .then(result=>{
                if(result){
                    res.json({"message":"person added successfully"});
                }else{
                    res.json({"message":"error while inserting person"});
                }
            });  
            // res.send(req_data);
        });
    }catch(err){
        res.send("error occured"+err.data);
    }
    
}

exports.testing_join=async (req,res)=>{
    /*single_person_data=await Person.query()
    .select()
    .whereExists(Person.relatedQuery('pets').where('pets.name', 'doggy1'));*/
    
    // get data by join owner_id=1 in pets table
    /*const dogs = await Person.relatedQuery('pets')
    // .for(1)//for owner_id=1
    .for([1,6])//for owner_id=1,6
    .where('pets.name', 'kutta')
    .orderBy('pets.name');*/

    //subquery
    /*const shubSubQry=Person.query().where('name','shubham');
    const persons_info=await Person.relatedQuery('pets')
    .for(shubSubQry)
    .orderBy('name');*/

    //inner joins
    /*data=await Person.query()
    .select('p.*')
    .innerJoinRelated('pets', { alias: 'p' })
    .where('p.owner_id',1);*/
    
    res.send("data");
    try{
        await Person.query()
        .insert({'name':'shiv'}).timeout(1000);
    }catch(err){
        let emailErrors = err.data.email;

        for (let i = 0; i < emailErrors.length; ++i) {
          let emailError = emailErrors[i];
      
          if (emailError.keyword === 'required') {
            console.log('This field is required!');
          } else if (emailError.keyword === 'minLength') {
            console.log('Must be longer than ' + emailError.params.limit);
          } else {
            console.log(emailError.message);
          }
        }
        // res.send("error occred");
    }
    //under testing
    //patch data
    /*await Pet.$query()
    .patch({ name: 'doggu' })
    .where('id',3);
    console.log('person updated');*/
    //insert 
    //const fluffy = await Person.$relatedQuery('pets').insert({ breed:"pug",name: 'Fluffy',gender:'0',owner_id:6 });
    // res.send(persons_info);
}

exports.validationEx=(req,res)=>{
    let naam="";
    /*const {username}=req.body;
    if(username){
        console.log("username exist");
    } */
    
    const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(400).json({
                success: false,
                errors: errors.array()
            });
        }

    let email="shu";
    if(email.length==0){
        message="email is required";
    }else{
        message="all okay";
    }
    res.send(message);
}

exports.send_mail=(req,res)=>{
    let fromMail = 'kumar.shubham031@gmail.com';
    let toMail = 'shubham.k@engineerbabu.in';
    let subject = 'I am a Bad Boy';
    let template_data=pug.renderFile(__dirname+'/'+"../views/email_template.pug",{encoding:'utf-8',from:fromMail,to:toMail,subject:subject,resp:'Click Here'})
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: fromMail ,
            pass: '9039888075s'
        }
    });
    let mailOptions = {
        from: fromMail,
        to: toMail,
        subject: subject,
        html: template_data
    };
    transporter.sendMail(mailOptions, (error, response) => {
        if (error) {
            console.log(error);
        }
        res.json(response);
        console.log(response)
    });
}

exports.template_view=(req,res)=>{
    // const compiledFunction = pug.compileFile(__dirname+'/../views/email_template.pug');
    // console.log(compiledFunction);
    res.render('email_template',{
        from:'kumar.shubham031@gmail.com',
        to:'shubham.k@engineerbabu.in',
        subject:'I am a bad boy',
        resp:'Click Here',
        img_name:__dirname+'/../../assets/images/car1.jpg'})
}

exports.sendgrid_template_view=(req,res)=>{
    // const compiledFunction = pug.compileFile(__dirname+'/../views/email_template.pug');
    // console.log(compiledFunction);
    let email_to='ritwik.n@supersourcing.com';
    res.render('sendgrid_template',{
        from:'kumar.shubham031@gmail.com',
        to:'shubham.k@engineerbabu.in',
        subject:'I am a bad boy',
        resp:'Click Here',
        img_name:__dirname+'/../../assets/images/car1.jpg',
        email_to:email_to,
        myname:'shubham',
        mycompany:'engineerbabu'
    })
}

const sendGridMail = require('@sendgrid/mail');
sendGridMail.setApiKey('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');

function getMessage() {
  const body = 'This is a test email using SendGrid from Node.js';
  return {
    to: 'kumar.shubham031@gmail.com',
    from: 'kumar.shubham031@gmail.com',
    subject: 'Test email with Node.js and SendGrid',
    text: body,
    html: `<strong>${body}</strong>`,
  };
}

exports.sendgrid_mailsend_func = async (req,res)=>{
    try {
        await sendGridMail.send(getMessage());
        console.log('Test email sent successfully');
    } 
    catch (error) {
        console.error('Error sending test email');
        console.error(error);
        if (error.response) {
            console.error(error.response.body)
        }
    }   
}

exports.sendgrid_mailsend_func1 = async (req,res)=>{
    let data=req.body;
    if(!data.myname){
        res.send("name is required");
    }else if(!data.mycompany){
        res.send("company name is required");
    }else{
        const sgMail = require('@sendgrid/mail')
        sgMail.setApiKey('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');
        
        // let email_to='pankaj.k@engineerbabu.in';
        let email_to=['pankaj.k@engineerbabu.in','shubham.k@engineerbabu.in'];
        let template_data=pug.renderFile(__dirname+'/'+"../views/sendgrid_template.pug",{encoding:'utf-8',email_to:email_to,myname:data.myname,mycompany:data.mycompany})
        
        const msg = {
            to: email_to, // Change to your recipient
            from: 'shubham.k@engineerbabu.in', // Change to your verified sender
            subject: 'Sending with SendGrid is Fun',
            text: 'and easy to do anywhere, even with Node.js',
            html: template_data,
            cc:['shivank.s@supersourcing.com','ritwik.n@supersourcing.com','zubearansari@gmail.com'],
        }
        sgMail
        .send(msg)
        .then(() => {
            res.send("email send successfully to "+email_to);
            console.log('Email sent')
        })
        .catch((error) => {
            console.error(error)
        })
    }
    
}