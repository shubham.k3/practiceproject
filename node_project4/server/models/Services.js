const { Model } = require('objection');
const conn=require('../conn/knex');
Model.knex(conn);
class Service extends Model {
    //static tableName = 'persons';
    static get tableName() {
        return 'services';
    }
    
    static get idColumn() {
        return 'id';
    }

    /*getPersonName(){
      return this.name;
    }*/
  
    static get relationMappings(){
        const Company=require('./Company');
        return {
          services: {
            relation: Model.ManyToManyRelation,
          //   modelClass: __dirname+'/Services',
            modelClass: Company,
            join: {
              from: 'services.id',
              through:{
                  from:'company_services.service_id',
                  to:'company_services.company_id'
              },
              to: 'company.id'
            }
          }
        }
    };

    static get jsonSchema() {
      return {
        type:'object',
        required:['service_name'],
        id:{type:['integer']},
        service_name:{type:'string',minLength:3,maxLength:255}
      }
    }


  }
  module.exports = Service;