const { Model } = require('objection');
const conn=require('../conn/knex');
Model.knex(conn);
class Person extends Model {
    //static tableName = 'persons';
    static get tableName() {
        return 'persons';
    }
    
    static get idColumn() {
        return 'id';
    }

    /*getPersonName(){
      return this.name;
    }*/
  
    static get relationMappings(){
      const Pet=require('./Pet');
      return {
        pets: {
          relation: Model.HasManyRelation,
          // modelClass: __dirname+'/Pet',
          modelClass: Pet,
          join: {
            from: 'persons.id',
            to: 'pets.owner_id'
          }
        }
      }
    };

    static get jsonSchema() {
      return {
        type:'object',
        required:['name','email','password','gender'],
        name:    {type:'string',minLength:5,maxLength:255},
        password:{type:'string',minLength:5,maxLength:255},
        email:   {type:'string',minLength:5,maxLength:255},
        gender: {type:'string',minLength:5,maxLength:255}
      }
    }


  }
  module.exports = Person;