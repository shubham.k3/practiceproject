const { Model } = require('objection');
const conn=require('../conn/knex');
Model.knex(conn);
class Company extends Model {
    //static tableName = 'persons';
    static get tableName() {
        return 'companys';
    }
    
    static get idColumn() {
        return 'id';
    }

    /*getPersonName(){
      return this.name;
    }*/
  
    static get relationMappings(){
      const Services=require('./Services');
      return {
        services: {
          relation: Model.ManyToManyRelation,
        //   modelClass: __dirname+'/Services',
          modelClass: Services,
          join: {
            from: 'companys.id',
            through:{
                from:'company_services.company_id',
                to:'company_services.service_id'
            },
            to: 'services.id'
          }
        }
      }
    };

    static get jsonSchema() {
        return {
          type:'object',
          required:['name','email','phone_no','address'],
          id:{type:['integer']},
          name:{type:'string',minLength:5,maxLength:255},
          email:{type:'string',minLength:5,maxLength:255},
          phone_no:{type:'string',minLength:10,maxLength:255},
          address:{type:'string',minLength:5,maxLength:255},
        }
    }


  }
  module.exports = Company;