const { Model } = require('objection');
const conn=require('../conn/knex');
Model.knex(conn);
class Team extends Model {
    //static tableName = 'persons';
    static get tableName() {
        return 'team';
    }
    
    static get idColumn() {
        return 'id';
    }

    static get jsonSchema() {
      return {
        type:'object',
        required:['email','name','password'],
        email:    {type:'string',minLength:5,maxLength:255},
        name:{type:'string',minLength:5,maxLength:255},
        password:   {type:'string',minLength:5,maxLength:255},
        usertoken: {type:'string',minLength:5,maxLength:255}
      }
    }


  }
  module.exports = Team;