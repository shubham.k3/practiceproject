const { Model } = require('objection');
const conn=require('../conn/knex');
Model.knex(conn);
class Eb_resources extends Model {
    //static tableName = 'eb_resources';
    static get tableName() {
        return 'eb_resources';
    }
    
    static get idColumn() {
        return 'id';
    }

    static get jsonSchema() {
      return {
        type:'object',
        required:['first_name','last_name','primary_skill','city','cost_price','sell_price','total_experiance','resume','availability','type'],
        properties:{
          id:{type:'integer'},
          first_name: {type:'string',minLength:1,maxLength:255},
          last_name: {type:'string',minLength:1,maxLength:255},
          primary_skill: {type:'string',minLength:1,maxLength:255},
          city: {type:'string',minLength:1,maxLength:255},
          cost_price: {type:'integer'},
          sell_price: {type:'integer'},
          total_experiance: {type:'string',minLength:1,maxLength:255},
          resume: {type:'string',minLength:1,maxLength:255},
          availability: {type:'string',minLength:1,maxLength:255},
          type: {type:'string',minLength:1,maxLength:255}
        }
        
      }
    }


  }
  module.exports = Eb_resources;

  /*

  address:{
            type:'object',
            properties:{
              city: {type:'string',minLength:1,maxLength:255},
              state: {type:'string',minLength:1,maxLength:255},
              country: {type:'string',minLength:1,maxLength:255},      
            }
          },
          */