const { Model } = require('objection');
const conn=require('../conn/knex');
Model.knex(conn);
class Pet extends Model {
  //static tableName = 'pets';
    static get tableName() {
        return 'pets';
    }
    
    static get idColumn() {
        return 'id';
    }
    
    static relationMappings() {
      const Person=require('./Person');
      return {
        owner: {
          relation: Model.BelongsToOneRelation,
          modelClass: Person,
          join: {
            from: 'pets.owner_id',
            to: 'persons.id'
          }
        }
      }
    };
  }
  module.exports = Pet;