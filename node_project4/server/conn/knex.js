var dbConfig = {
    client: 'mysql',
    connection: {
      host     : 'localhost',
      user     : 'root',
      password : '',
      database : 'node_task1'
    }
  };
  var knex = require('knex')(dbConfig);
  module.exports = knex;