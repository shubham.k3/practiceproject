module.exports = {
    adminAccess : (req,res,next) => {
        if(!req.body.user){
            res.send("you are not authorised");
            // throw globalCalls.forbiddenError("you are not authorised");
        }else if(req.body.user=='admin'){
            return next();
        }else{
            res.send("you are not authorised");
        }
    },
    userAccess : (req,res,next) => {
        if(!req.user){
            res.send("you are not authorised");
        }
        if(req.user.user_type=='user'){
            return next();
        }else{
            res.send("you are not authorised");
        }
    },
    clientAccess : (req,res,next) => {
        if(!req.user){
            res.send("you are not authorised");
        }
        if(req.user.user_type=='client'){
            return next();
        }else{
            res.send("you are not authorised");
        }
    },
    customerAccess : (req,res,next) => {
        if(!req.user){
            res.send("you are not authorised");
            console.log("unauthorised customer");
        }
        if(req.user.user_type=='customer'){
            return next();
        }else{
            res.send("you are not authorised");
        }
    }
    
}