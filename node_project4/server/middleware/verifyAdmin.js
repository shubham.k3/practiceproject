const SECREAT="SECREAT_HAI";
const jwt = require('jsonwebtoken')
module.exports = (req,res,next) => {
  // res.setHeader('Authorization',req.body.token);
  /*if(!req.headers['Authorization']){
    return res.status(401).json({"error": true, "message": 'Auth error.' });
  }*/
    const token = req.body.token || req.query.token || req.headers['authorization']
    // decode token
    if (token) {
      // verifies secret and checks exp
      jwt.verify(token, SECREAT, function(err, decoded) {
          if (err) {
              return res.status(401).json({"error": true, "message": 'Unauthorized access.' });
          }
        req.decoded = decoded;
        next();
      });
    } else {
      // if there is no token
      // return an error
      return res.status(403).send({
          "error": true,
          "message": 'No token provided.'
      });
    }
  }