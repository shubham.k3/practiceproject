const multer=require('multer');
const image_path=multer({dest:'/assets/images'});
const path=require('path');

const imageStorage =multer.diskStorage({
    destination : 'assets/images',
    filename:(req,file,cb)=>{
        img_name=file.fieldname + '-' + Date.now() + path.extname(file.originalname);
        // console.log(file.fieldname + '-' + Date.now() + path.extname(file.originalname));
        cb(null,file.fieldname+'-'+Date.now()+path.extname(file.originalname))
    }
});

const imageUpload=multer({
    storage:imageStorage,
    limit:{fileSize:15000000},// 15 MB
    fileFilter(req,file,cb){
        if(!file.originalname.match(/\.(png|jpg|jpeg|svg)$/)){
            return cb(new Error('please upload image'))
        }
        cb(undefined,true)
    }
}).single('profile_image');

exports.uploadaImage = (req,res,next) => {
    imageUpload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
        res.send(err);
        } else if (err) {
        res.send(err);
        }else{
            console.log("image uploaded successfully");
            next();
            // res.send("uploaded"+req.body.message);
        }
    });
}