const SECREAT = "secret hai";
const jwt = require('jsonwebtoken')
module.exports = (req,res,next) => {
 const token = req.body.token || req.query.token || req.headers['authorization']
    // decode token
    if (token) {
      // verifies secret and checks exp
      let new_token=req.headers.authorization.split(" ")[1];
      jwt.verify(new_token, SECREAT, function(err, decoded) {
          if (err) {
              return res.status(401).json({"error": true, "message": 'Unauthorized access.' });
          }
        req.user_id = decoded.user_id;
        next();
      });
    } else {
      // if there is no token
      return res.status(403).send({
          "error": true,
          "message": 'No token provided.'
      });
    }
  }